var request = require('request'),
structs = require('./structs'),
db_categorias = require('./models/cate'),
db_entidad = require('./models/enti');
module.exports = {
	recived_message: function(evento,token,url){
		sender_id = evento.sender.id;
		recipient_id = evento.recipient.id;
		time_message = evento.timestamp;
		message = evento.message;
		text = message.text;
		
		_this = this;


		/*
		GUARDAR TODOS LOS MENSAJES
		*/
		console.log(evento);


		_this.call_send_API(structs.typing_message(sender_id),token,url);
		db_entidad.findOne(sender_id,function(usuario){
			if(usuario!=null){
				if(usuario.enabled==true){







					//_this.call_send_API(structs.text_message(sender_id,'Hola de nuevo '+usuario.first_name),token,url);
					db_categorias.show(function(categorias){
						categorias = JSON.parse(categorias);
						var categorias_parse = [];
						categorias.forEach(function(item){
							categorias_parse.push({
								title: item.name,
								payload: 'CATEGORIA_'+item._id,
								img: item.picture
							});
						});
						_this.call_send_API(structs.quick_reply(sender_id,'Estas son nuestras categorias',categorias_parse),token,url);
					});





				}
			}else{
				_this.call_user_API(sender_id,token,url,function(user){
					user._id = sender_id;
					db_entidad.create(user);
					_this.call_send_API(structs.text_message(sender_id,'Bienvenido '+user.first_name),token,url);
				});
			}
		});
	},
	call_send_API: function(data,token,url){
		console.log(data);
		request({
			url: url+'me/messages',
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			json: data,
			qs: {
				access_token: token
			}
		},function(error, response, body){
			if (!error && response.statusCode == 200) {
				console.log('Mensaje enviado con exito!');
			}
		});
	},
	call_user_API: function(user_id,token,url,callback){
		request({
			url: url+user_id,
			method: 'GET',
			headers: {
				'Content-type': 'application/json'
			},
			qs: {
				access_token: token
			}
		},function(error, response, body){
			if (!error && response.statusCode == 200) {
				callback(JSON.parse(body));
			}
		});
	}
};