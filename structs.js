module.exports = {
	typing_message: function(recipient_id){
		message_data = {
			recipient: {
				id: recipient_id
			},
			sender_action: 'typing_on'
		};
		return message_data;
	},
	text_message: function(recipient_id,message_text){
		message_data = {
			recipient: {
				id: recipient_id
			},
			message: {
				text: message_text
			}
		};
		return message_data;
	},
	quick_reply: function(recipient_id,message_text,items){
		var _this = this,
		item_res = [];
		items.forEach(function(it){
			item_res.push(_this.item_quick_reply(it));
		});
		message_data = {
			recipient: {
				id: recipient_id
			},
			message: {
				text: message_text,
				quick_replies: item_res
			}
		};
		return message_data;
	},
	item_quick_reply: function(item){
		var item_res = {
			"content_type":"text",
			title: item.title,
			payload: item.payload,
			image_url: item.img
		};
		return item_res;
	}
};