var mongoose = require('mongoose'),
Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/chat');

var cate_schema = new Schema({
	_id: String,
	name: {type: String,required:[true,'El nombre es obligatorio']},
	descr: String,
	picture: String
});
cate_model = mongoose.model('categoria', cate_schema, 'categorias');

module.exports = {
	show: function(callback){
		cate_model.find({},function(err,items){
			if (!err) {
				callback(JSON.stringify(items));
			}else{
				return console.log(err);
			}
		});
	},
	create: function(data,callback){
		var item = {
			_id: data._id,
			name: data.name,
			descr: data.descr,
			picture: data.picture
		};
		var nuevo = new cate_model(item).save(function(err){
			if(err!=null){
				var txt = '';
				for(x in err.errors){
					var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
					console.log(error_tmp);
					txt += error_tmp+'\n';
				}
				callback({
					error: txt
				});
			}
		}).then(function(doc){
			callback(doc);
		});
	},
	update: function(data,callback){
		cate_model.findOne({_id: data._id},function(err,item){
			item.name = data.name;
			item.descr = data.descr;
			item.picture = data.picture;
			item.save(function(err){
				if(err!=null){
					var txt = '';
					for(x in err.errors){
						var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
						console.log(error_tmp);
						txt += error_tmp+'\n';
					}
					callback({
						error: txt
					});
				}
			}).then(function(doc){
				callback(doc);
			});
		});
	},
	delete: function(_id,callback){
		cate_model.findOne({_id: _id},function(err,post){
			post.remove();
			callback(_id);
		});
	}
};