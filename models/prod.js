var mongoose = require('mongoose'),
Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/chat');

var prod_schema = new Schema({
	_id: String,
	name: {type: String,required:[true,'El nombre es obligatorio']},
	descr: String,
	category: Object,
	picture: String
});
prod_model = mongoose.model('producto', prod_schema, 'productos');

module.exports = {
	show: function(callback){
		prod_model.find({},function(err,items){
			if (!err) {
				callback(JSON.stringify(items));
			}else{
				return console.log(err);
			}
		});
	},
	create: function(data,callback){
		var item = {
			_id: data._id,
			name: data.name,
			descr: data.descr,
			category: data.category,
			picture: data.picture
		};
		var nuevo = new prod_model(item).save(function(err){
			if(err!=null){
				var txt = '';
				for(x in err.errors){
					var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
					console.log(error_tmp);
					txt += error_tmp+'\n';
				}
				callback({
					error: txt
				});
			}
		}).then(function(doc){
			callback(doc);
		});
	},
	update: function(data,callback){
		prod_model.findOne({_id: data._id},function(err,item){
			item.name = data.name;
			item.descr = data.descr;
			item.category = data.category;
			item.picture = data.picture;
			item.save(function(err){
				if(err!=null){
					var txt = '';
					for(x in err.errors){
						var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
						console.log(error_tmp);
						txt += error_tmp+'\n';
					}
					callback({
						error: txt
					});
				}
			}).then(function(doc){
				callback(doc);
			});
		});
	},
	delete: function(_id,callback){
		prod_model.findOne({_id: _id},function(err,post){
			post.remove();
			callback(_id);
		});
	}
};