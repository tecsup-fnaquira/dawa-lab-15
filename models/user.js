var mongoose = require('mongoose'),
Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/chat');

var user_schema = new Schema({
	_id: String,
	email: String,
	password: String
});
user_model = mongoose.model('user', user_schema,'users');

module.exports = {
	show: function(callback){
		user_model.find({},function(err,items){
			if (!err) {
				callback(JSON.stringify(items));
			}else{
				return console.log(err);
			}
		});
	},
	findMail: function(email,password,callback){
		user_model.findOne({email:email,password:password},function(err,item){
			if (!err) {
				callback(item);
			}else{
				return console.log(err);
			}
		});
	},
	create: function(data,callback){
		var item = {
			_id: data._id,
			email: data.email,
			password: data.password
		};
		var nuevo = new user_model(item).save(function(err){
			if(err!=null){
				var txt = '';
				for(x in err.errors){
					var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
					console.log(error_tmp);
					txt += error_tmp+'\n';
				}
				callback({
					error: txt
				});
			}
		}).then(function(doc){
			callback(doc);
		});
	},
	update: function(data,callback){
		user_model.findOne({_id: data._id},function(err,item){
			item.email = data.email;
			item.password = data.password;
			item.save(function(err){
				if(err!=null){
					var txt = '';
					for(x in err.errors){
						var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
						console.log(error_tmp);
						txt += error_tmp+'\n';
					}
					callback({
						error: txt
					});
				}
			}).then(function(doc){
				callback(doc);
			});
		});
	},
	delete: function(_id,callback){
		user_model.findOne({_id: _id},function(err,post){
			post.remove();
			callback(_id);
		});
	}
};