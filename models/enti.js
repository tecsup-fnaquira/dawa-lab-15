var mongoose = require('mongoose'),
Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/chat');

var enti_schema = new Schema({
	_id: String,
	first_name: {type: String,required:[true,'El primer nombre es obligatorio']},
	last_name: String,
	timezone: {type: Number,min:[-24,'Debe ser minimo -24'],max:[24,'No debe ser mayor a 24']},
	locale: {type: String,enum: ['es_LA','en_US','es_ES']},
	profile_pic: String,
	enabled: Boolean
});
enti_model = mongoose.model('entidades', enti_schema,'entidades');

module.exports = {
	show: function(callback){
		enti_model.find({},function(err,items){
			if (!err) {
				callback(JSON.stringify(items));
			}else{
				return console.log(err);
			}
		});
	},
	findOne: function(id,callback){
		enti_model.findById(id,function(err,item){
			if (!err) {
				callback(item);
			}else{
				return console.log(err);
			}
		});
	},
	create: function(data,callback){
		var item = {
			_id: data._id,
			first_name: data.first_name,
			last_name: data.last_name,
			timezone: data.timezone,
			locale: data.locale,
			profile_pic: data.profile_pic,
			enabled: true
		};
		var nuevo = new enti_model(item).save(function(err){
			if(err!=null){
				var txt = '';
				for(x in err.errors){
					var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
					console.log(error_tmp);
					txt += error_tmp+'\n';
				}
				callback({
					error: txt
				});
			}
		}).then(function(doc){
			callback(doc);
		});
	},
	update: function(data,callback){
		enti_model.findOne({_id: data._id},function(err,item){
			item.first_name = data.first_name;
			item.last_name = data.last_name;
			item.timezone = data.timezone;
			item.locale = data.locale;
			item.profile_pic = data.profile_pic;
			item.save(function(err){
				if(err!=null){
					var txt = '';
					for(x in err.errors){
						var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
						console.log(error_tmp);
						txt += error_tmp+'\n';
					}
					callback({
						error: txt
					});
				}
			}).then(function(doc){
				callback(doc);
			});
		});
	},
	delete: function(_id,callback){
		enti_model.findOne({_id: _id},function(err,post){
			post.remove();
			callback(_id);
		});
	}
};