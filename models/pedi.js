var mongoose = require('mongoose'),
Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/chat');

var pedi_schema = new Schema({
	_id: String,
	user: String,
	producto: String,
	total: Number
});
pedi_model = mongoose.model('pedido', pedi_schema, 'pedidos');

module.exports = {
	show: function(callback){
		pedi_model.find({},function(err,items){
			if (!err) {
				callback(JSON.stringify(items));
			}else{
				return console.log(err);
			}
		});
	},
	create: function(data,callback){
		var item = {
			_id: data._id,
			user: data.user,
			producto: data.producto,
			total: data.total
		};
		var nuevo = new pedi_model(item).save(function(err){
			if(err!=null){
				var txt = '';
				for(x in err.errors){
					var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
					console.log(error_tmp);
					txt += error_tmp+'\n';
				}
				callback({
					error: txt
				});
			}
		}).then(function(doc){
			callback(doc);
		});
	},
	update: function(data,callback){
		pedi_model.findOne({_id: data._id},function(err,item){
			item.user = data.user;
			item.producto = data.producto;
			item.total = data.total;
			item.save(function(err){
				if(err!=null){
					var txt = '';
					for(x in err.errors){
						var error_tmp = err.errors[x].path+'=> '+err.errors[x].message;
						console.log(error_tmp);
						txt += error_tmp+'\n';
					}
					callback({
						error: txt
					});
				}
			}).then(function(doc){
				callback(doc);
			});
		});
	},
	delete: function(_id,callback){
		pedi_model.findOne({_id: _id},function(err,post){
			post.remove();
			callback(_id);
		});
	}
};