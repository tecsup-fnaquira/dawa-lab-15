$(document).ready(function(){
	var socket = io(),
	fill = function(data){
		if($('#'+data._id).length==0){
			var $row = $('<tr id="'+data._id+'">');
			$row.append('<td>'+data._id+'</td>');
			$row.append('<td>'+data.user+'</td>');
			$row.append('<td>'+data.producto+'</td>');
			$row.append('<td>'+data.total+'</td>');
			$row.append('<td><button class="btn btn-success btn-sm" name="btnAct">Actualizar</button></td>');
			$row.append('<td><button class="btn btn-danger btn-sm" name="btnEli">Eliminar</button></td>');
			$row.data('data',data);
			$row.find('[name=btnAct]').click(function(){
				var data = $(this).closest('tr').data('data');
				$('#_id').val(data._id);
				$('#user').val(data.user);
				$('#producto').val(data.producto);
				$('#total').val(data.total);
				$('.warning').removeClass('warning');
				$(this).closest('tr').addClass('warning');
			});
			$row.find('[name=btnEli]').click(function(){
				var _id = $(this).closest('tr').attr('id');
				if (confirm('Continuar con eliminacion de registro?')) {
					socket.emit('eliminar pedido',_id);
				}
			});
			$('table tbody').append($row);
		}else{
			var $row = $('#'+data._id);
			$row.find('td:eq(1)').html(data.user);
			$row.find('td:eq(2)').html(data.producto);
			$row.find('td:eq(3)').html(data.total);
			$row.data('data',data);
		}
	};
	socket.emit('get pedidos');
	$('#formulario').submit(function(e){
		e.preventDefault();
		var data = {
			_id: $('#_id').val(),
			user: $('#user').val(),
			producto: $('#producto').val(),
			total: $('#total').val()
		};
		var accion = 'crear pedido';
		if($('.warning').length>0) accion = 'actualizar pedido';
		$('.warning').removeClass('warning');
		socket.emit(accion,data);
		$('#formulario').trigger('reset');
		return true;
	});
	socket.on('listar pedidos',function(data){
		data = JSON.parse(data);
		for(var i=0,j=data.length; i<j; i++){
			fill(data[i]);
		}
	});
	socket.on('nuevo pedido',function(data){
		if(data.error!=null){
			return alert(data.error);
		}
		fill(data);
	});
	socket.on('borrar pedido',function(data){
		$('#'+data).remove();
	});
});