$(document).ready(function(){
	var socket = io(),
	fill = function(data){
		if($('#'+data._id).length==0){
			var $row = $('<tr id="'+data._id+'">');
			$row.append('<td>'+data._id+'</td>');
			$row.append('<td>'+data.name+'</td>');
			$row.append('<td>'+data.descr+'</td>');
			$row.append('<td>'+data.category.name+'</td>');
			$row.append('<td>'+data.picture+'</td>');
			$row.append('<td><button class="btn btn-success btn-sm" name="btnAct">Actualizar</button></td>');
			$row.append('<td><button class="btn btn-danger btn-sm" name="btnEli">Eliminar</button></td>');
			$row.data('data',data);
			$row.find('[name=btnAct]').click(function(){
				var data = $(this).closest('tr').data('data');
				$('#_id').val(data._id);
				$('#name').val(data.name);
				$('#descr').val(data.descr);
				$('#category').val(data.category._id);
				$('#picture').val(data.picture);
				$('.warning').removeClass('warning');
				$(this).closest('tr').addClass('warning');
			});
			$row.find('[name=btnEli]').click(function(){
				var _id = $(this).closest('tr').attr('id');
				if (confirm('Continuar con eliminacion de registro?')) {
					socket.emit('eliminar producto',_id);
				}
			});
			$('table tbody').append($row);
		}else{
			var $row = $('#'+data._id);
			$row.find('td:eq(1)').html(data.name);
			$row.find('td:eq(2)').html(data.descr);
			$row.find('td:eq(3)').html(data.category.name);
			$row.find('td:eq(4)').html(data.picture);
			$row.data('data',data);
		}
	};
	socket.emit('get productos');
	socket.emit('get categorias');
	$('#formulario').submit(function(e){
		e.preventDefault();
		var data = {
			_id: $('#_id').val(),
			name: $('#name').val(),
			descr: $('#descr').val(),
			category: {
				_id: $('#category').val(),
				name: $('#category option:selected').html()
			},
			picture: $('#picture').val()
		};
		if(data._id==''){
			$('#_id').focus();
			return alert('Debe ingresar un ID!');
		}
		if(data.name==''){
			$('#name').focus();
			return alert('Debe ingresar un nombre!');
		}
		var accion = 'crear producto';
		if($('.warning').length>0) accion = 'actualizar producto';
		$('.warning').removeClass('warning');
		socket.emit(accion,data);
		$('#formulario').trigger('reset');
		return true;
	});
	socket.on('listar categorias',function(data){
		data = JSON.parse(data);
		var $cbo = $('#category').empty();
		for(var i=0,j=data.length; i<j; i++){
			$cbo.append('<option value="'+data[i]._id+'">'+data[i].name+'</option>');
		}
	});
	socket.on('listar productos',function(data){
		data = JSON.parse(data);
		for(var i=0,j=data.length; i<j; i++){
			fill(data[i]);
		}
	});
	socket.on('nuevo producto',function(data){
		if(data.error!=null){
			return alert(data.error);
		}
		fill(data);
	});
	socket.on('borrar producto',function(data){
		$('#'+data).remove();
	});
});