module.exports = [
	{
		context: 'welcome',
		type: 'text',
		title: 'Bienvenido',
		text: 'Bienvenido a nuestra tienda',
		order: 1
	},
	{
		context: 'welcome',
		type: 'text',
		title: 'Registrado',
		text: 'Ahora eres parte de nuestra base de datos',
		order: 2
	},
	{
		context: 'welcome',
		type: 'text',
		title: 'Registrado',
		text: 'Este es mi tercer mensaje',
		order: 3
	},
	{
		context: 'second_greeting',
		type: 'text',
		title: 'Registrado',
		text: 'Este es mi tercer mensaje',
		order: 1
	}
];