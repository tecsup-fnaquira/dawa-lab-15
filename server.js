var express = require('express'),
app = express(),
http = require('http').Server(app),
io = require('socket.io')(http),
port = process.env.PORT || 3000,
bodyParser = require('body-parser'),
session = require('express-session'),
config = require('./config'),
handler = require('./handler'),
session_middleware = require('./middlewares/session'),
db_user = require('./models/user'),
db_cate = require('./models/cate'),
db_prod = require('./models/prod'),
db_pedi = require('./models/pedi'),
data_msgs = require('./data/messages');







/*
REEMPLAZAR ESTE BLOQUE POR FUNCIONES DE MODELO
*/
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/chat');

var msgs_schema = new Schema({
	context: String,
	type: String,
	title: String,
	text: String,
	order: Number
});
msgs_model = mongoose.model('message', msgs_schema, 'messages');
msgs_model.remove();
data_msgs.forEach(function(item){
	new msgs_model(item).save(function(err){
		console.log(err);
	});
});









app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine','jade');

app.use(session({
	secret: 'tecsup_rules',
	resave: false,
	saveUninitialized: false
}));

app.use('/static', express.static('public'));

app.get('/login', function(req, res){
	res.render('login');
});

app.post('/login',function(req,res){
	db_user.findMail(req.body.email,req.body.password,function(data){
		if(data==null){
			res.redirect('/login');
		}else{
			req.session.user = data;
			res.redirect('/');
		}
	});
});

app.get('/logout',function(req,res){
	req.session.destroy();
	res.redirect('/login');
});

app.get('/webhook',function(req,res){
	if(req.query['hub.verify_token']==config.SECURITY){
		res.send(req.query['hub.challenge']);
	}else{
		res.send('Error');
	}
});

app.post('/webhook',function(req,res){
	req.body.entry.forEach(function(entry){
		entry.messaging.forEach(function(message_event){
			if(message_event.message!=null){
				handler.recived_message(message_event,config.PAGE_ACCESS_TOKEN,config.URL_API);
			}
		});
	});
	res.send('ok');
});

app.use('/',session_middleware);

app.get('/', function(req, res){
	res.render('main');
});
app.get('/categorias', function(req, res){
	res.render('categorias');
});
app.get('/productos', function(req, res){
	res.render('productos');
});
app.get('/pedidos', function(req, res){
	res.render('pedidos');
});

io.on('connection', function(socket){
	console.log('Usuario conectado!');


	/****************************************
	* <!-- CATEGORIAS
	****************************************/
	socket.on('get categorias',function(){
		db_cate.show(function(data){
			socket.emit('listar categorias',data);
		});
	});
	socket.on('crear categoria',function(data){
		db_cate.create(data,function(rpta){
			io.emit('nueva categoria',rpta);
		});
	});
	socket.on('actualizar categoria',function(data){
		db_cate.update(data,function(rpta){
			io.emit('nueva categoria',rpta);
		});
	});
	socket.on('eliminar categoria',function(data){
		db_cate.delete(data,function(rpta){
			io.emit('borrar categoria',rpta);
		});
	});
	/****************************************
	* CATEGORIAS -->
	****************************************/


	/****************************************
	* <!-- PRODUCTOS
	****************************************/
	socket.on('get productos',function(){
		db_prod.show(function(data){
			socket.emit('listar productos',data);
		});
	});
	socket.on('crear producto',function(data){
		db_prod.create(data,function(rpta){
			io.emit('nuevo producto',rpta);
		});
	});
	socket.on('actualizar producto',function(data){
		db_prod.update(data,function(rpta){
			io.emit('nuevo producto',rpta);
		});
	});
	socket.on('eliminar producto',function(data){
		db_prod.delete(data,function(rpta){
			io.emit('borrar producto',rpta);
		});
	});
	/****************************************
	* PRODUCTOS -->
	****************************************/


	/****************************************
	* <!-- PEDIDOS
	****************************************/
	socket.on('get pedidos',function(){
		db_pedi.show(function(data){
			socket.emit('listar pedidos',data);
		});
	});
	socket.on('crear pedido',function(data){
		db_pedi.create(data,function(rpta){
			io.emit('nuevo pedido',rpta);
		});
	});
	socket.on('actualizar pedido',function(data){
		db_pedi.update(data,function(rpta){
			io.emit('nuevo pedido',rpta);
		});
	});
	socket.on('eliminar pedido',function(data){
		db_pedi.delete(data,function(rpta){
			io.emit('borrar pedido',rpta);
		});
	});
	/****************************************
	* PEDIDOS -->
	****************************************/


	socket.on('disconnect', function () {
		console.log('Usuario desconectado!');
	});
});


http.listen(port, function(){
	console.log('Servidor conectado en *:' + port);
});